package com.xueyi.common.core.constant;

/**
 * 权限相关通用常量
 *
 * @author xueyi
 */
public class SecurityConstants {

    /** 企业Id字段 */
    public static final String DETAILS_ENTERPRISE_ID = "enterprise_id";

    /** 企业账号字段 */
    public static final String DETAILS_ENTERPRISE_NAME = "enterprise_name";

    /** 企业类型字段 */
    public static final String DETAILS_IS_LESSOR = "is_lessor";

    /** 用户Id字段 */
    public static final String DETAILS_USER_ID = "user_id";

    /** 用户账号字段 */
    public static final String DETAILS_USERNAME = "user_name";

    /** 用户类型字段 */
    public static final String DETAILS_TYPE = "user_type";

    /** 授权信息字段 */
    public static final String AUTHORIZATION_HEADER = "authorization";

    /** 请求来源 */
    public static final String FROM_SOURCE = "from-source";

    /** 内部请求 */
    public static final String INNER = "inner";

    /** 用户标识 */
    public static final String USER_KEY = "user_key";

    /** 登录用户 */
    public static final String LOGIN_USER = "login_user";
}
